<?php
  function removeNonDigits(string $str): string{
    return preg_replace("/[^0-9]/", "", $str);
  }

  echo removeNonDigits("Text - 123 - Text - 456");