<?php
class NestedTreeBuilder {
  private $flatCategories = [];
  private $nestedCategories = [];

  public function addCategories(int $id, int $parent_id, string $name): void {
    $this->flatCategories[] = ["id" => $id, "parent_id" => $parent_id, "name" => $name];
  }

  public function printFlatCategories(): void {
    echo "<pre>";
    print_r($this->flatCategories);
    echo "</pre>";
  }

  public function printNestedCategories(): void {
    echo "<pre>";
    print_r($this->nestedCategories);
    echo "</pre>";
  }

  public function buildNestedTree(): void {
    $childFlatCategories = [];

    foreach($this->flatCategories as $index => $category) {
      if ($category["parent_id"] === 0) {
        $category["children"] = [];
        $this->nestedCategories[] = $category;
      } else {
        $childFlatCategories[] = $category;
      }
    }

    foreach($this->nestedCategories as $index => $parentCategory) {
      $this->nestedCategories[$index] = $this->addChildToParent($parentCategory, $childFlatCategories);
    }
  }

  private function addChildToParent(array $parent, array $flatCategories): array {
    if (count($flatCategories) === 0) {
      return [];
    }

    foreach($flatCategories as $index => $flatCategory) {
      if ($parent["id"] === $flatCategory["parent_id"]) {
        $parent["children"][$index] = $flatCategory;
        unset($flatCategories[$index]);
        $parent["children"][$index] = $this->addChildToParent($parent["children"][$index], $flatCategories);
      }
    }
    return $parent;
  }
}

$ntb = new NestedTreeBuilder();

$ntb->addCategories(1, 0, "Elektronika");
$ntb->addCategories(2, 6, "test");
$ntb->addCategories(3, 1, "Roboty");
$ntb->addCategories(4, 6, "Piłka nożna");
$ntb->addCategories(5, 0, "Turystyka");
$ntb->addCategories(6, 0, "Sport");
$ntb->addCategories(7, 1, "Telefony");
$ntb->addCategories(8, 1, "Laptopy");
$ntb->addCategories(9, 1, "Tablety");
$ntb->addCategories(10,6, "Siłownia i fitness");

//$ntb->printFlatCategories();

$ntb->buildNestedTree();
$ntb->printNestedCategories();