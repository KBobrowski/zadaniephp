<?php

abstract class Product {
  protected $size = null;

  function __construct(string $size) {
    $this->size = $size;
  }

  final public function printSize(): void{
    if ($this instanceof Pants) {
      echo "Pants size is " . $this->size;
    } else if ($this instanceof TShirt) {
      echo "T-Shirt size is " . $this->size;
    }
  }
}

class Pants extends Product {
  // public function printSize() {
  //   echo "test";
  // }
}

class TShirt extends Product {

}

$pants = new Pants("M");
$pants->printSize();
echo "<br>";
$tShirt = new TShirt("S");
$tShirt->printSize();