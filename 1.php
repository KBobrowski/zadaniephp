<?php
if (isset($_GET['a']) && isset($_GET['b'])) {
  $a = $_GET["a"];
  $b = $_GET["b"];

  $isOccurred = strpos($b, $a);
  if ($isOccurred !== false) {
    echo "a is part of b";
  } else {
    echo "a is not part of b";
  }
}