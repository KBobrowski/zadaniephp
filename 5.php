<?php

class ProductsManage {
  private $products = [];

  public function addProduct(string $productName, float $grossPrice, int $quantity, string $VATRate): void{
    $netPrice = round($grossPrice / ($this->parseVATMultiplier($VATRate) + 1), 2);
    $product = [
      "name" => $productName,
      "grossPrice" => $grossPrice,
      "quantity" => $quantity,
      "VAT" => $VATRate,
      "netPrice" => $netPrice,
      "grossSum" => $grossPrice * $quantity,
      "netSum" => $netPrice * $quantity,
    ];

    array_push($this->products, $product);
  }

  private function parseVATMultiplier ($VATRate): float {
    if ($VATRate !== "8%" || $VATRate !== "23%") {
      return (float)preg_replace("/[^0-9]/", "", $VATRate) / 100;
    } else {
      return 0;
    }
  }

  public function calcRaport(): void{
    $raportArr = [];

    foreach($this->products as $index => $product) {
      if (!isset($raportArr[$product["VAT"]])) {
        $raportArr[$product["VAT"]] = [];
        $raportArr[$product["VAT"]]["netSum"] = 0;
        $raportArr[$product["VAT"]]["VATSum"] = 0;
        $raportArr[$product["VAT"]]["grossSum"] = 0;
      }

      $raportArr[$product["VAT"]]["netSum"] += $product["netSum"];
      $raportArr[$product["VAT"]]["VATSum"] += ($product["grossSum"] - $product["netSum"]);
      $raportArr[$product["VAT"]]["grossSum"] += $product["grossSum"];
    }

    echo '<pre>'; print_r($raportArr); echo '</pre>';
  }

  public function printProducts(): void{
    echo '<pre>'; print_r($this->products); echo '</pre>';
  }
}

$pm = new ProductsManage();
$pm->addProduct("Product 1", 100.33,  5, "23%");
$pm->addProduct("Product 2", 3.79,    3, "8%");
$pm->addProduct("Product 3", 99.11,   1, "23%");
$pm->addProduct("Product 4", 77.17,   7, "23%");
$pm->addProduct("Product 5", 14.22,   7, "8%");
$pm->addProduct("Product 6", 17.99,   11,"0%");
$pm->addProduct("Product 7", 17.99,   11,"zw");
$pm->addProduct("Product 8", 20.99,   7, "zw");
//$pm->printProducts();
$pm->calcRaport();