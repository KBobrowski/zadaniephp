<?php

$array = [
  "key1" => [
    "key11" => "value11",
    "key12" => "value12",
    "key13" => "value13",
  ],
  "key2" => [
    "key21" => "value21",
    "key22" => "value22",
    "key23" => "value23",
  ],
  "key3" => [
    "key31" => "value31",
    "key32" => "value32",
    "key33" => "value33",
  ],
];

$obj = json_decode(json_encode($array));
