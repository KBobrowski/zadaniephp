<?php

class Singleton {
  private static $instance = null;
  private $name = null;

  private function __construct () {}

  public static function getInstance(): object {
    if (self::$instance) {
      return self::$instance;
    }
    self::$instance = new Singleton();
    return self::$instance;
  }

  public function getName(): string{
    return $this->name;
  }

  public function setName(string $name): void{
    $this->name = $name;
  }
}

$singleton = Singleton::getInstance();
$singleton2 = Singleton::getInstance();

$singleton->setName("Name");

echo $singleton->getName();
echo "<br>";
echo $singleton2->getName();

echo "<br>";
$singleton2->setName("Name 2");

echo $singleton->getName();
echo "<br>";
echo $singleton2->getName();


echo "<br> --- <br>";
echo var_dump($singleton === $singleton2);