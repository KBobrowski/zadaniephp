<?php
$start = 100;
$end = 0;
$text = "";
$tmp = "";

for ($i = $start; $i >= $end; $i--) {

  if ($i % 15 === 0) {
    $tmp = $i . " good excellent";
    echo $tmp. "<br>";
  } else if ($i % 3 === 0) {
    $tmp = $i . " good";
    echo $tmp . "<br>";
  } else if ($i % 5 === 0) {
    $tmp = $i . " excellent";
    echo $tmp . "<br>";
  } else {
    $tmp = $i . " ok";
    echo  $tmp . "<br>";
  }
  
  if ($i === ($end + 10)) {
    $tmp = $i . " stop!";
    echo  $tmp . "<br>";
    $text .= $tmp . "\n";
    break;
  } else {
    $text .= $tmp . "\n";
  }
}

file_put_contents("text.txt", $text);