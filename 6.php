<?php
function buildCategoryArray(array $categoryStrings, string $delimiter = ">"): array {
  $rawCategories = [];
  foreach($categoryStrings as $index => $str) {
    $str = str_replace(' ', '', $str);
    $explodedCategories = explode($delimiter, $str);

    $rawCategories[] = array_reduce(array_reverse($explodedCategories), function($prevArray, $key){
      return $prevArray ? [$key => $prevArray] : [$key => []];
    }, null);
  }
  return array_merge_recursive(...$rawCategories);
}
$textData = [
  "Komputery > Laptopy > Akcesoria > Torby",
  "Komputery > Laptopy > Myszki",
  "Monitory > LCD > 15",
  "Komputery > Stacjonarne > Dell",
  "Pozostałe"
];

echo "<pre>";
var_dump(buildCategoryArray($textData, ">"));
echo "</pre>";
